import { PrismaClient, Prisma, Role } from "@prisma/client";
import { hashPassword } from "../src/lib/hash";

const prisma = new PrismaClient();

const users: Prisma.UserCreateInput[] = [
  {
    username: "admin",
    password: "123456",
    role: Role.ADMIN,
  },
  {
    username: "user1",
    password: "123456",
    role: Role.USER,
  },
  {
    username: "user2",
    password: "123456",
    role: Role.USER,
  },
  {
    username: "user3",
    password: "123456",
    role: Role.USER,
  },
];

const main = async () => {
  for (const user of users) {
    console.log(`Upserting User: ${user.username}`);

    const hashedPassword = await hashPassword(user.password);
    await prisma.user.upsert({
      where: { username: user.username },
      update: {},
      create: {
        ...user,
        password: hashedPassword,
      },
    });
  }
};

main()
  .catch((e) => {
    console.log(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
