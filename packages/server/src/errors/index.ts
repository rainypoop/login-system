export class ApiError extends Error {
  statusCode = 400;

  constructor(message) {
    super(message);
    this.name = "ApiError";
    this.statusCode = 400;
  }
}

export class NotFoundError extends ApiError {
  statusCode = 404;

  constructor(message) {
    super(message);
    this.name = "NotFoundError";
  }
}

export class AuthenticationError extends ApiError {
  statusCode = 401;

  constructor(message) {
    super(message);
    this.name = "AuthenticationError";
  }
}

export class TokenError extends ApiError {
  statusCode = 403;

  constructor(message) {
    super(message);
    this.name = "AuthenticationError";
  }
}