import { QueryDto, UsersQueryDto } from "@login/common";
import { Router } from "express";
import asyncHandler from "express-async-handler";
import { RequireJwt, RequireRole } from "../middlewares/requireJwt";
import { getUserById, getUsers } from "../controllers/users";

const userRouter = Router();

userRouter.get(
  "/",
  RequireRole("ADMIN"),
  asyncHandler(async (req, res) => {
    const query = await QueryDto.parseAsync(req.query);
    const userSearch = await UsersQueryDto.parseAsync(req.query);
    const user = await getUsers(query, userSearch);
    res.status(200).json(user);
  }),
);

userRouter.get(
  "/:userId",
  RequireJwt,
  asyncHandler(async (req, res) => {
    const { userId } = req.params;
    const user = await getUserById(userId);
    delete user.password;
    res.status(200).json(user);
  }),
);

export { userRouter };
