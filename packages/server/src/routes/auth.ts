import { Router } from "express";
import asyncHandler from "express-async-handler";
import { login } from "../controllers/auth";
import { LoginDto } from "@login/common";

const authRouter = Router();

authRouter.post(
  "/login",
  asyncHandler(async (req, res) => {
    const loginDto = await LoginDto.parseAsync(req.body);
    const user = await login(loginDto.username, loginDto.password);
    res.status(200).json(user);
  }),
);

export { authRouter };
