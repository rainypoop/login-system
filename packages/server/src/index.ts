import { PrismaClient } from "@prisma/client";
import express, { Request, Response } from "express";
import { AppConfig } from "./lib/config";
import { ApiErrorHandler } from "./middlewares/apiErrorHandler";
import { authRouter } from "./routes/auth";
import { userRouter } from "./routes/users";

export const prisma = new PrismaClient();

const app = express();

async function main() {
  app.use(express.json());

  app.use("/users", userRouter);
  app.use("/auth", authRouter);

  // Catch unregistered routes
  app.all("*", (req: Request, res: Response) => {
    res.status(404).json({ error: `Route ${req.originalUrl} not found` });
  });

  app.use(ApiErrorHandler);

  app.listen(AppConfig.port, () => {
    console.log(`
    🚀 Server ready at: http://localhost:${AppConfig.port}`);
  });
}

main()
  .then(async () => {
    await prisma.$connect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
  });
