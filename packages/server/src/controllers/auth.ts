import { TokenDto } from "@login/common";
import { TokenError } from "../errors";
import { verifyHash } from "../lib/hash";
import { signToken } from "../lib/jwt";
import { getUserByUsername } from "./users";

export const login = async (
  username: string,
  password: string,
): Promise<TokenDto> => {
  const user = await getUserByUsername(username);

  if (!(await verifyHash(password, user.password))) {
    throw new TokenError("Invalid credentials");
  }

  delete user.password;

  return { accessToken: signToken(user) };
};
