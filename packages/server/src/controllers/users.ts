import {
  QueryDto,
  UserEntity,
  UserManyDto,
  UsersQueryDto,
} from "@login/common";
import { Prisma } from "@prisma/client";
import { NotFoundError } from "../errors";
import { prisma } from "../index";

export const getUsers = async (
  query: QueryDto,
  userSearch: UsersQueryDto,
): Promise<UserManyDto> => {
  let where: Prisma.UserWhereInput = {
    role: {
      notIn: ["ADMIN"],
    },
  };

  if (userSearch.username) {
    where = {
      ...where,
      username: {
        contains: userSearch.username,
      },
    };
  }

  const { page, perPage } = query;

  const skip: number = perPage * (page - 1);
  const [totalCount, results] = await prisma.$transaction([
    prisma.user.count({ where }),
    prisma.user.findMany({
      skip,
      take: perPage,
      select: {
        id: true,
        username: true,
        role: true,
        createdAt: true,
        updatedAt: true,
      },
      where,
    }),
  ]);

  const totalPages = Math.ceil(totalCount / perPage);

  return {
    page,
    perPage,
    prevPage: page - 1 < 1 ? null : page - 1,
    nextPage: page + 1 > totalPages ? null : page + 1,
    totalCount,
    totalPages,
    results,
  };
};

export const getUserById = async (id: string): Promise<UserEntity> => {
  const user = await prisma.user.findUnique({
    where: { id },
  });

  if (!user) {
    throw new NotFoundError("User not found");
  } else {
    return user;
  }
};

export const getUserByUsername = async (
  username: string,
): Promise<UserEntity> => {
  const user = await prisma.user.findUnique({
    where: { username },
  });

  if (!user) {
    throw new NotFoundError("User not found");
  } else {
    return user;
  }
};
