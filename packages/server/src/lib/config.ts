interface Config {
  secret: string;
  port: number;
  globalPrefix: string;
}

export const AppConfig: Config = {
  secret: process.env.SERVER_SECRET || "",
  port: parseInt(process.env.SERVER_PORT) || 8000,
  globalPrefix: process.env.SERVER_GLOBAL_PREFIX || "",
};
