import { UserDto } from "@login/common";
import { sign, verify } from "jsonwebtoken";
import { TokenError } from "../errors";
import { AppConfig } from "./config";

export const signToken = (user: UserDto) =>
  sign(UserDto.parse(user), AppConfig.secret);
export const verifyToken = (token: string) => {
  try {
    return UserDto.parse(verify(token, AppConfig.secret));
  } catch (error) {
    throw new TokenError("Invalid token");
  }
};
