import { hash, verify } from "argon2";

export const hashPassword = async (
  str: string,
): Promise<string> => {
  return await hash(str);
};

export const verifyHash = async (
  str: string,
  hashedStr: string,
): Promise<boolean> => {
  return await verify(hashedStr, str);
};
