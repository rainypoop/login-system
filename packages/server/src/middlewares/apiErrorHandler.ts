import type { ErrorRequestHandler } from "express";
import { ApiError } from "../errors";

export const ApiErrorHandler: ErrorRequestHandler = (
  err: ApiError,
  req,
  res,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  next,
) => {
  const errStatus = err.statusCode || 500;
  const errMsg = err.message || "Something went wrong";
  res.status(errStatus).json({
    success: false,
    status: errStatus,
    message: errMsg,
  });
};
