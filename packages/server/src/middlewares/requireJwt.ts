import { RoleEnum, UserDto } from "@login/common";
import { Handler, Request } from "express";
import { TokenError } from "../errors";
import { verifyToken } from "../lib/jwt";

export interface RequestWithUser extends Request {
  user: UserDto;
}

export const RequireJwt: Handler = (req: RequestWithUser, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null) {
    throw new TokenError("Token missing");
  }

  const user = verifyToken(token);
  req.user = user;

  return next();
};

export const RequireRole: (role: RoleEnum) => Handler =
  (role: RoleEnum) => (req: RequestWithUser, res, next) => {
    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];

    if (token == null) {
      throw new TokenError("Token missing");
    }

    const user = verifyToken(token);
    req.user = user;

    if (user.role != role) {
      throw new TokenError("Invalid Role");
    }

    return next();
  };
