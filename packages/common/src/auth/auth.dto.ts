import { Username, Password } from "../users/user.helper";
import { z } from "zod";

export const LoginDto = z.object({
  username: Username,
  password: Password,
});

export const TokenDto = z.object({
  accessToken: z.string(),
});


export type LoginDto = z.infer<typeof LoginDto>;
export type TokenDto = z.infer<typeof TokenDto>;
