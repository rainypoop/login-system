import { z } from "zod";

export function createPaginatedResponseSchema<ItemType extends z.ZodTypeAny>(
  itemSchema: ItemType,
) {
  return z.object({
    page: z.number().int(),
    perPage: z.number().int(),
    prevPage: z.number().int().nullable(),
    nextPage: z.number().int().nullable(),
    totalCount: z.number(),
    totalPages: z.number(),
    results: z.array(itemSchema),
  });
}
