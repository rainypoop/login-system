import { z } from "zod";

export const QueryDto = z.object({
  page: z.coerce.number().int().min(1).default(1),
  perPage: z.coerce.number().int().default(10),
});

export const SearchQueryDto = z.object({
  q: z.string().optional(),
});

export type QueryDto = z.infer<typeof QueryDto>
export type SearchQueryDto = z.infer<typeof SearchQueryDto>