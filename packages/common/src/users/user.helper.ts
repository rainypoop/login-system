import { z } from "zod";

export const Username = z.string().min(4);
export const Password = z.string().min(6);