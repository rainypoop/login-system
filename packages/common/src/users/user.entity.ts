import { RoleEnum } from "../roles";
import { z } from "zod";
import { Password, Username } from "./user.helper";

export const UserEntity = z.object({
  id: z.string(),
  username: Username,
  password: Password,
  role: RoleEnum,
  createdAt: z.coerce.date(),
  updatedAt: z.coerce.date(),
});

export type UserEntity = z.infer<typeof UserEntity>