import { SearchQueryDto } from "../paginated";
import { createPaginatedResponseSchema } from "../paginated";
import { CreatableRoleEnum, RoleEnum } from "../roles";
import { z } from "zod";
import { Password, Username } from "./user.helper";

// Zod validator definitions
export const UserDto = z.object({
  id: z.string(),
  username: Username,
  role: RoleEnum,
  createdAt: z.coerce.date(),
  updatedAt: z.coerce.date(),
});

export const UserManyDto = createPaginatedResponseSchema(UserDto);

export const CreateUserDto = z.object({
  username: Username,
  password: Password,
  role: CreatableRoleEnum,
});

export const UsersQueryDto = SearchQueryDto.extend({
  username: z.string().optional()
});

// Type exports
export type UserDto = z.infer<typeof UserDto>;
export type CreateUserDto = z.infer<typeof CreateUserDto>;
export type UserManyDto = z.infer<typeof UserManyDto>
export type UsersQueryDto = z.infer<typeof UsersQueryDto>
