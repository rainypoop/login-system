import { z } from "zod";

export const RoleEnum = z.enum(["ADMIN", "USER"]);
export const CreatableRoleEnum = z.enum(["USER"]);

export type RoleEnum = z.infer<typeof RoleEnum>
export type CreatableRoleEnum = z.infer<typeof CreatableRoleEnum>